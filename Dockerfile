FROM python:latest

RUN export DISPLAY=:10

RUN apt-get update \
&& apt-get install -y dbus-x11 \
&& export $(dbus-launch) # export the dbus

RUN apt-get update \
&& apt-get install -y inkscape ghostscript

RUN apt-get update \
&& apt-get install -y texlive texlive-lang-czechslovak texlive-latex-extra texlive-fonts-extra \
&& fc-cache -f -v

COPY fonts/* /usr/local/share/fonts/
RUN fc-cache -f -v

RUN pip install --upgrade pip
RUN pip install virtualenv && virtualenv venv
